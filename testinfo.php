<html>
    <head>
        <title>  </title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <form >
            <table border="2" align="center">
                <tr>
                    <th>
                        Ingredientes
                    </th>
                    <th>
                        Peso B. (em Kg)
                    </th>
                    <th>
                        Vlr. Carboidrato
                    </th>
                    <th>
                        Vlr. Proteína
                    </th>
                    <th>
                        Vlr. Kcal
                    </th>
                </tr>
                <tr align="center">
                    <td>
                        Farinha de Trigo
                    </td>
                    <td>
                        1
                    </td>
                    <td>
                        0,740
                    </td>
                    <td>
                        0,100
                    </td>
                    <td>
                        3480
                    </td>
                </tr>
                <tr align="center">
                    <td>
                        Margarina
                    </td>
                    <td>
                        1
                    </td>
                    <td>
                        0,007
                    </td>
                    <td>
                        0,002
                    </td>
                    <td>
                        7170
                    </td>
                </tr>
                <tr align="center">
                    <td>
                        Açucar
                    </td>
                    <td>
                        1
                    </td>
                    <td>
                        0,996
                    </td>
                    <td>
                        0,0032
                    </td>
                    <td>
                        3868
                    </td>
                </tr>
                <tr align="center">
                    <td>
                        Cenoura
                    </td>
                    <td>
                        1
                    </td>
                    <td>
                        34
                    </td>
                    <td>
                        19
                    </td>
                    <td>
                        200
                    </td>
                </tr>

                <tr align="center">
                    <td>
                        Ovo
                    </td>
                    <td>
                        1
                    </td>
                    <td>
                        0,72
                    </td>
                    <td>
                        5,85
                    </td>
                    <td>
                        64,35
                    </td>
                </tr>

                <tr align="center">
                    <td>
                        Leite
                    </td>
                    <td>
                        1
                    </td>
                    <td>
                        9,4
                    </td>
                    <td>
                        6,6
                    </td>
                    <td>
                        118
                    </td>
                </tr>

                <tr align="center">
                    <td>
                        Fermento em pó
                    </td>
                    <td>
                        0,2
                    </td>
                    <td>
                        1,32
                    </td>
                    <td>
                        0,02
                    </td>
                    <td>
                        5,36
                    </td>
                </tr>
            </table>
            <br>
        <br>
        <br>
        <div>
            Alunos: Wagner, Raquel, Iago
            <br>
            <br>
            Aqui temos uma tabela de exemplo de uma das funções da FTP,
            uma vez que a apresentação de informações nutricionais são
            necessarias para calculos.
            <br>
            <br>
            PS: as paginas de calculos apresentaram erros durante o desenvolvimento/commit
            por isso não estão sendo apresentadas ainda.
        </div>
        </form>
            
    </body>
</html>
